﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace Flight_Simulator
{
    public partial class MainWindow : Form
    {
        ShopForm shopForm;
        HangarForm hangarForm;
        Game Game;
        private int SecondsToAdd, tmpTime;
        List<ListDate> ListDate;
        Random rnd = new Random(0);

        public MainWindow()
        {
            InitializeComponent();
            Game = new Game();
            shopForm = new ShopForm(Game);
            hangarForm = new HangarForm(Game);
            ListDate = new List<ListDate>();
            InitFlight();
            InitMyFlight();
            lblTime.Text = Game.GetTime.ToString();
            lblMoney.Text = Game.GetMoneyStr();
            SecondsToAdd = tmpTime=1;
        }

        // отображение на форму доступных рейсов
        private void InitFlight()
        {
            ListDate.Clear();
            lviAvailableFlights.Items.Clear();
            int n = Game.GetCountFlight();
            string[,] s = Game.GetFlight();
            for (int i = 0; i < n; i++)
            {
                string dist = Game.DistanceBetweenCities(s[0, i], s[1, i]).ToString();
                lviAvailableFlights.Items.Add(new ListViewItem(new[] {s[0, i], s[1, i], s[2, i], s[3, i], s[6, i], s[7, i], s[8, i], dist}, 0) {Tag = s[4, i]});
                ListDate.Add(new ListDate(int.Parse(s[4, i]), DateTime.Parse(s[5, i])));
            }
            lviAvailableFlights.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lviAvailableFlights.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            lviAvailableFlights.Sort();
            ListComparer lc = new ListComparer();
            ListDate.Sort(lc);
        }

        private void InitMyFlight()
        {
            lviMyFlights.Items.Clear();
            int n = Game.GetCountMyFlight();
            string[,] s = Game.GetMyFlight();
            for (int i = 0; i < n; i++)
            {
                string dist = Game.DistanceBetweenCities(s[0, i], s[1, i]).ToString();
                if (s[5, i] != "")
                    lviMyFlights.Items.Add(new ListViewItem(new[] {s[0, i], s[1, i], s[2, i], s[3, i],s[10,i], s[6, i], s[7, i], s[8, i], Game.GetPlaneName(s[5, i]), dist,s[9,i]}, 0) {Tag = s[4, i]});
                else
                    lviMyFlights.Items.Add(new ListViewItem(new[] {s[0, i], s[1, i], s[2, i], s[3, i],s[10,i], s[6, i], s[7, i], s[8, i], "Самолет не назначен", dist,"Ожидается"}, 0) {Tag = s[4, i]});
            }
            lviMyFlights.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lviMyFlights.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void btnShop_Click(object sender, EventArgs e)
        {
            shopForm.ShowDialog();
            Game.InitMoney();
        }

        private void btnHangar_Click(object sender, EventArgs e)
        {
            hangarForm.ShowDialog();
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            hangarForm.Dispose();
            shopForm.Dispose();
            Game.SaveTimeAndMoney();
            ListDate.Clear();
        }

        //обновление label со временем
        private void timerToTime_Tick(object sender, EventArgs e)
        {
            if (Game.GetMoney <= 0 && Game.GetCountMyBuePlanes() == 0 && SecondsToAdd != 0)
            {
                tmpTime = SecondsToAdd;
                SecondsToAdd = 0;
                if (MessageBox.Show("Вы проиграли, Начать новую игру?", "Lose", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    Game.NewGame();
                    ContinuesTime();
                }
                else
                {
                    Application.Exit();
                }
            }
            else
            {
                lblTime.Text = Game.GetTime.ToString();
                lblMoney.Text = Game.GetMoneyStr();
                while (ListDate.Count != 0 && ListDate[0].Date <= Game.GetTime)
                {
                    Game.DeleteFlight(ListDate[0].id);
                    int i = 0;
                    while (i < lviAvailableFlights.Items.Count)
                    {
                        if (int.Parse(lviAvailableFlights.Items[i].Tag.ToString()) == ListDate[0].id)
                            lviAvailableFlights.Items.RemoveAt(i);
                        i++;
                    }
                    ListDate.RemoveAt(0);
                }
                Game.ArendPay();
                Game.LeasingPay();
                Game.InitMoney();
            }
        }

        //игровое время
        private void GameTime_Tick(object sender, EventArgs e)
        {
            if (tmpTime != 0)
                Game.GetTime = Game.GetTime.AddSeconds(SecondsToAdd);
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            SecondsToAdd = 1;
        }

        private void pauseTime()
        {
            tmpTime = SecondsToAdd;
            SecondsToAdd = 0;
        }

        private void ContinuesTime()
        {
            SecondsToAdd = tmpTime;
        }

        private void btnX60_Click(object sender, EventArgs e)
        {
            SecondsToAdd = 60;
        }

        private void btnX3600_Click(object sender, EventArgs e)
        {
            SecondsToAdd = 3600;
        }

        private void новаяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Game.NewGame();
            InitMyFlight();
            InitFlight();
            lblTime.Text = Game.GetTime.ToString();
            lblMoney.Text = Game.GetMoneyStr();
            SecondsToAdd = 1;
        }

        private void timerFlights_Tick(object sender, EventArgs e)
        {
            if (tmpTime != 0)
            {
                //в случае, если осталось меньше 5 доступных рейсов, генерируется от 1 до 10 новых
                if (Game.GetCountFlight() < 5)
                {
                    Game.NewFlight(rnd.Next(5, 15));
                    InitFlight();
                }
                //удаление старых рейсов из доступных рейсов ???????
                while (ListDate.Count != 0 && ListDate[0].Date <= Game.GetTime)
                {
                    Game.DeleteFlight(ListDate[0].id);
                    int i = 0;
                    while (i < lviAvailableFlights.Items.Count)
                    {
                        if (int.Parse(lviAvailableFlights.Items[i].Tag.ToString()) == ListDate[0].id)
                            lviAvailableFlights.Items.RemoveAt(i);
                        i++;
                    }
                    ListDate.RemoveAt(0);
                }
                //удаление старых рейсов из моих рейсов
                for (int i = 0; i < lviMyFlights.Items.Count; i++)
                {
                    if (lviMyFlights.Items.Count > i && Convert.ToDateTime(lviMyFlights.Items[i].SubItems[3].Text) <= Game.GetTime && lviMyFlights.Items[i].SubItems[10].Text == "Ожидается") //если рейс просрочен
                    {
                        Game.DeleteFlight(Convert.ToInt32(lviMyFlights.Items[i].Tag)); //удаление рейса из бд
                        Game.DeleteMyFlight(Convert.ToInt32(lviMyFlights.Items[i].Tag)); //удаление моего рейса из бд
                        if (lviMyFlights.Items[i].SubItems[8].Text == "Самолет не назначен")
                        {
                            Game.GetMoney -= int.Parse(lviMyFlights.Items[i].SubItems[6].Text);
                            Game.UpdateMoney(Game.GetMoney);
                        }
                        else
                        {
                            Game.GetMoney += int.Parse(lviMyFlights.Items[i].SubItems[6].Text);
                            Game.UpdateMoney(Game.GetMoney);
                        }
                        lviMyFlights.Items.RemoveAt(i);
                    }
                    //отправление в полет со статусом "в ангаре"
                    if (lviMyFlights.Items.Count > i && lviMyFlights.Items[i].SubItems[10].Text == "В ангаре" && Convert.ToDateTime(lviMyFlights.Items[i].SubItems[3].Text) <= Game.GetTime)
                    {
                        Game.ChangeStatus(int.Parse(lviMyFlights.Items[i].Tag.ToString()));
                        lviMyFlights.Items[i].SubItems[10].Text = "В рейсе";
                    }
                    //удаление выполненных рейсов
                    if (lviMyFlights.Items.Count > i && lviMyFlights.Items[i].SubItems[10].Text == "В рейсе" && Convert.ToDateTime(lviMyFlights.Items[i].SubItems[4].Text) <= Game.GetTime)
                    {
                        if (lviMyFlights.Items[i].SubItems[7].Text != "Туда-обратно")
                        {
                            Game.UpdatePlace(int.Parse(lviMyFlights.Items[i].Tag.ToString()), lviMyFlights.Items[i].SubItems[1].Text);
                            Game.DeleteFlight(Convert.ToInt32(lviMyFlights.Items[i].Tag)); //удаление рейса из бд
                            Game.DeleteMyFlight(Convert.ToInt32(lviMyFlights.Items[i].Tag)); //удаление моего рейса из бд
                            Game.GetMoney += int.Parse(lviMyFlights.Items[i].SubItems[6].Text);
                            Game.UpdateMoney(Game.GetMoney);
                        }
                        else
                        {
                            Game.DeleteFlight(Convert.ToInt32(lviMyFlights.Items[i].Tag)); //удаление рейса из бд
                            Game.DeleteMyFlight(Convert.ToInt32(lviMyFlights.Items[i].Tag)); //удаление моего рейса из бд
                            Game.GetMoney += int.Parse(lviMyFlights.Items[i].SubItems[6].Text);
                            Game.UpdateMoney(Game.GetMoney);
                        }
                        lviMyFlights.Items.RemoveAt(i);
                    }
                }
                Game.InitMoney();
                lblMoney.Text = Game.GetMoneyStr();
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            pauseTime();
            GoToCity g = new GoToCity(Game);
            g.ShowDialog();
            InitMyFlight();
            ContinuesTime();
        }

        private void взятьРейсToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lviAvailableFlights.SelectedItems.Count != 0)
            {
                Game.AddMyFlight(int.Parse(lviAvailableFlights.SelectedItems[0].Tag.ToString()));
                InitMyFlight();
                InitFlight();
            }
            else
                MessageBox.Show(@"Сначала выберите рейс");
        }

        private void назначитьСамолетToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pauseTime();
            if (lviMyFlights.SelectedItems.Count != 0)
            {
                PlaneSelect pSelect = new PlaneSelect();
                pSelect.flightID = int.Parse(lviMyFlights.SelectedItems[0].Tag.ToString());
                pSelect.distance = Game.DistanceBetweenCities(lviMyFlights.SelectedItems[0].SubItems[0].Text, lviMyFlights.SelectedItems[0].SubItems[1].Text);
                pSelect.from = lviMyFlights.SelectedItems[0].SubItems[0].Text;
                pSelect.ShowDialog();
                pSelect.InitPlanes();
                InitMyFlight();
            }
            else
                MessageBox.Show(@"Сначала выберите рейс");
            ContinuesTime();
        }

        private void btnX86400_Click(object sender, EventArgs e)
        {
            SecondsToAdd = 86400;
        }

        private void отказатьсяОтРейсаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lviMyFlights.SelectedItems.Count != 0)
            {
                Game.DeleteMyFlight(int.Parse(lviMyFlights.SelectedItems[0].Tag.ToString())); //удаление рейса из моих рейсов
                Game.DeleteFlight(int.Parse(lviMyFlights.SelectedItems[0].Tag.ToString())); //удаление рейса из всех рейсов
                Game.MyMoney -= int.Parse(lviMyFlights.SelectedItems[0].SubItems[6].Text); //уменьшение баланса (неустойка)
                lblMoney.Text = Game.GetMoneyStr();
                MessageBox.Show(@"Вы отказались от рейса" + '\n' + @"С Вас была списана неустойка в размере " + int.Parse(lviMyFlights.SelectedItems[0].SubItems[6].Text) + '$');
                InitMyFlight();
            }
            else
                MessageBox.Show(@"Сначала выберите рейс");
        }
    }
}