﻿namespace Flight_Simulator
{
    partial class ShopForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnBuy = new System.Windows.Forms.Button();
            this.btnRent = new System.Windows.Forms.Button();
            this.btnLeasing = new System.Windows.Forms.Button();
            this.lblMoney = new System.Windows.Forms.Label();
            this.lviPlanes = new System.Windows.Forms.ListView();
            this.PlaneName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Cost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Carrying = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PasCapacity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Distance = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Tenancy = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Expense = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timerToTime = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblName = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblCost = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblCostArenda = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numUPDArenda = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblCostLizing = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numUPDLizing = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUPDArenda)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUPDLizing)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBuy
            // 
            this.btnBuy.Location = new System.Drawing.Point(158, 48);
            this.btnBuy.Name = "btnBuy";
            this.btnBuy.Size = new System.Drawing.Size(94, 21);
            this.btnBuy.TabIndex = 1;
            this.btnBuy.Text = "Купить";
            this.btnBuy.UseVisualStyleBackColor = true;
            this.btnBuy.Click += new System.EventHandler(this.btnBuy_Click);
            // 
            // btnRent
            // 
            this.btnRent.Location = new System.Drawing.Point(158, 48);
            this.btnRent.Name = "btnRent";
            this.btnRent.Size = new System.Drawing.Size(94, 23);
            this.btnRent.TabIndex = 3;
            this.btnRent.Text = "Взять в аренду";
            this.btnRent.UseVisualStyleBackColor = true;
            this.btnRent.Click += new System.EventHandler(this.btnRent_Click);
            // 
            // btnLeasing
            // 
            this.btnLeasing.Location = new System.Drawing.Point(158, 46);
            this.btnLeasing.Name = "btnLeasing";
            this.btnLeasing.Size = new System.Drawing.Size(94, 23);
            this.btnLeasing.TabIndex = 4;
            this.btnLeasing.Text = "Взять в лизинг";
            this.btnLeasing.UseVisualStyleBackColor = true;
            this.btnLeasing.Click += new System.EventHandler(this.btnLeasing_Click);
            // 
            // lblMoney
            // 
            this.lblMoney.AutoSize = true;
            this.lblMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblMoney.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblMoney.Location = new System.Drawing.Point(725, 0);
            this.lblMoney.Name = "lblMoney";
            this.lblMoney.Size = new System.Drawing.Size(49, 13);
            this.lblMoney.TabIndex = 6;
            this.lblMoney.Text = "lblMoney";
            // 
            // lviPlanes
            // 
            this.lviPlanes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.PlaneName,
            this.Cost,
            this.Carrying,
            this.PasCapacity,
            this.Distance,
            this.Tenancy,
            this.Expense});
            this.lviPlanes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lviPlanes.FullRowSelect = true;
            this.lviPlanes.GridLines = true;
            this.lviPlanes.Location = new System.Drawing.Point(0, 0);
            this.lviPlanes.MultiSelect = false;
            this.lviPlanes.Name = "lviPlanes";
            this.lviPlanes.Size = new System.Drawing.Size(774, 308);
            this.lviPlanes.TabIndex = 8;
            this.lviPlanes.UseCompatibleStateImageBehavior = false;
            this.lviPlanes.View = System.Windows.Forms.View.Details;
            this.lviPlanes.SelectedIndexChanged += new System.EventHandler(this.lviPlanes_SelectedIndexChanged);
            // 
            // PlaneName
            // 
            this.PlaneName.Text = "Название";
            // 
            // Cost
            // 
            this.Cost.Text = "Стоимость";
            // 
            // Carrying
            // 
            this.Carrying.Text = "Грузоподъемность";
            // 
            // PasCapacity
            // 
            this.PasCapacity.Text = "Кол-во пассажиров";
            // 
            // Distance
            // 
            this.Distance.Text = "Максимальная дальность";
            // 
            // Tenancy
            // 
            this.Tenancy.Text = "Аренда в месяц";
            this.Tenancy.Width = 63;
            // 
            // Expense
            // 
            this.Expense.Text = "Издержки за км";
            // 
            // timerToTime
            // 
            this.timerToTime.Enabled = true;
            this.timerToTime.Interval = 1000;
            this.timerToTime.Tick += new System.EventHandler(this.timerToTime_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Время: ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblMoney);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(774, 19);
            this.panel1.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Location = new System.Drawing.Point(677, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Деньги:";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTime.Location = new System.Drawing.Point(46, 0);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(40, 13);
            this.lblTime.TabIndex = 10;
            this.lblTime.Text = "lblTime";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lviPlanes);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(774, 308);
            this.panel2.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblName);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lblCost);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.btnBuy);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 327);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(258, 77);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Покупка";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(66, 25);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(0, 13);
            this.lblName.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Самолет:";
            // 
            // lblCost
            // 
            this.lblCost.AutoSize = true;
            this.lblCost.Location = new System.Drawing.Point(46, 51);
            this.lblCost.Name = "lblCost";
            this.lblCost.Size = new System.Drawing.Size(0, 13);
            this.lblCost.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Цена:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblCostArenda);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.numUPDArenda);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.btnRent);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(258, 327);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(258, 77);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Аренда";
            // 
            // lblCostArenda
            // 
            this.lblCostArenda.AutoSize = true;
            this.lblCostArenda.Location = new System.Drawing.Point(84, 53);
            this.lblCostArenda.Name = "lblCostArenda";
            this.lblCostArenda.Size = new System.Drawing.Size(0, 13);
            this.lblCostArenda.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Итого:";
            // 
            // numUPDArenda
            // 
            this.numUPDArenda.Location = new System.Drawing.Point(128, 23);
            this.numUPDArenda.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numUPDArenda.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUPDArenda.Name = "numUPDArenda";
            this.numUPDArenda.Size = new System.Drawing.Size(124, 20);
            this.numUPDArenda.TabIndex = 5;
            this.numUPDArenda.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUPDArenda.ValueChanged += new System.EventHandler(this.numUPDArenda_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Количество месяцев:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblCostLizing);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.numUPDLizing);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.btnLeasing);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(516, 327);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(258, 77);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Лизинг";
            // 
            // lblCostLizing
            // 
            this.lblCostLizing.AutoSize = true;
            this.lblCostLizing.Location = new System.Drawing.Point(84, 53);
            this.lblCostLizing.Name = "lblCostLizing";
            this.lblCostLizing.Size = new System.Drawing.Size(0, 13);
            this.lblCostLizing.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Цена в месяц:";
            // 
            // numUPDLizing
            // 
            this.numUPDLizing.Location = new System.Drawing.Point(128, 23);
            this.numUPDLizing.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUPDLizing.Name = "numUPDLizing";
            this.numUPDLizing.Size = new System.Drawing.Size(124, 20);
            this.numUPDLizing.TabIndex = 7;
            this.numUPDLizing.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUPDLizing.ValueChanged += new System.EventHandler(this.numUPDLizing_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Количество месяцев:";
            // 
            // ShopForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 404);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ShopForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Магазин";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ShopForm_FormClosing);
            this.Load += new System.EventHandler(this.ShopForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUPDArenda)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUPDLizing)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnBuy;
        private System.Windows.Forms.Button btnRent;
        private System.Windows.Forms.Button btnLeasing;
        private System.Windows.Forms.Label lblMoney;
        private System.Windows.Forms.ListView lviPlanes;
        private System.Windows.Forms.ColumnHeader PlaneName;
        private System.Windows.Forms.ColumnHeader Cost;
        private System.Windows.Forms.ColumnHeader Carrying;
        private System.Windows.Forms.ColumnHeader PasCapacity;
        private System.Windows.Forms.ColumnHeader Distance;
        private System.Windows.Forms.ColumnHeader Tenancy;
        private System.Windows.Forms.ColumnHeader Expense;
        private System.Windows.Forms.Timer timerToTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblCost;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblCostArenda;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numUPDArenda;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblCostLizing;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numUPDLizing;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label label8;
    }
}