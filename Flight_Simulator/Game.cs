﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Flight_Simulator
{
    public class Game
    {
        Random rnd = new Random();
        DateTime MyDate; // дата
        public BigInteger MyMoney = 0; //деньги
        BigInteger FirstMoney = 1000000000;
        SQLiteConnection sConn; // строка подключения

        public BigInteger GetMoney
        {
            get { return MyMoney; }
            set { MyMoney = value; }
        }

        public string GetMoneyStr()
        {
            return "$" + MyMoney;
        }

        public DateTime GetTime
        {
            get { return MyDate; }
            set { MyDate = value; }
        }

        public Game()
        {
            StartConnection();
            InitTimeAndMoney();
        }

        public void StartConnection()
        {
            String StringConnection = @"Data Source = DataBase.sqlite;Version=3;";
            sConn = new SQLiteConnection(StringConnection);
            try
            {
                sConn.Open();
            }
            catch
            {
                MessageBox.Show("Соединение с базой данных не установлено");
            }
        }

        // инициализация из бд денег и времени текущего(игрового)
        private void InitTimeAndMoney()
        {
            SQLiteCommand sCommand = new SQLiteCommand // запрос в бд за данными о юзере
            {
                Connection = sConn,
                CommandText = @"SELECT COUNT (*) FROM User "
            };
            if (int.Parse(sCommand.ExecuteScalar().ToString()) == 0) // проверка на пустоту в таблице User
            {
                SQLiteCommand insertDate = new SQLiteCommand // занесения первоначальных данных (новая игра)
                {
                    Connection = sConn,
                    CommandText = @"INSERT INTO User (Date,Money) VALUES (@Date,@Money)",
                };
                insertDate.Parameters.AddWithValue("@Date", DateTime.Now);
                insertDate.Parameters.AddWithValue("@Money", FirstMoney);
                insertDate.ExecuteNonQuery();
                MyDate = DateTime.Now;
                MyMoney = FirstMoney;
                NewFlight(15);
            }
            else
            {
                SQLiteCommand select = new SQLiteCommand // вывод имеющихся данных (Продолжение игры)
                {
                    Connection = sConn,
                    CommandText = @"SELECT Date AS 'date', Money AS 'money' FROM User",
                };
                SQLiteDataReader reader = select.ExecuteReader();
                while (reader.Read())
                {
                    MyDate = Convert.ToDateTime(reader[0]);
                    MyMoney = BigInteger.Parse(reader[1].ToString());
                }
            }
        }

        //вывод имеющихся данных (Продолжение игры)
        public void InitMoney()
        {
            SQLiteCommand select = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT Money AS 'money' FROM User",
            };
            SQLiteDataReader reader = select.ExecuteReader();
            while (reader.Read())
                MyMoney = BigInteger.Parse(reader[0].ToString());
        }

        public void UpdateMoney(BigInteger MoN)
        {
            SQLiteCommand update = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"update user set money = @money",
            };
            update.Parameters.AddWithValue("@money", MoN);
            update.ExecuteNonQuery();
        }

        // Удаление рейса по id
        public void DeleteFlight(int id)
        {
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"DELETE FROM Flights WHERE Flights.id = @ID"
            };
            sCommand.Parameters.AddWithValue("@ID", id);
            sCommand.ExecuteNonQuery();
        }

        // сохранение прогресса в бд
        public void SaveTimeAndMoney()
        {
            //очистка таблицы User и занесение в нее времени и денег
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"DELETE FROM User; INSERT INTO User VALUES (@Date, @Money)"
            };
            sCommand.Parameters.AddWithValue("@Date", GetTime);
            sCommand.Parameters.AddWithValue("@Money", MyMoney);
            sCommand.ExecuteNonQuery();
        }

        //создание N новых рейсов
        public void NewFlight(int N)
        {
            //получение типов рейса (регулярный или чартерный)
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select * FROM TypeFlight",
            };
            SQLiteDataReader reader = sCommand.ExecuteReader();
            int[] typeFlightsIDs = new int[2];
            int i = 0;
            while (reader.Read())
                typeFlightsIDs[i++] = Convert.ToInt32(reader[0]);
            sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select count (*) FROM Directions",
            };
            int c = int.Parse(sCommand.ExecuteScalar().ToString());
            sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select id FROM Directions",
            };
            reader = sCommand.ExecuteReader();
            int[] idRoute = new int[c];
            i = 0;
            while (reader.Read())
                idRoute[i++] = Convert.ToInt32(reader[0]);
            for (int j = 0; j < N; j++)
            {
                int h1 = rnd.Next(MyDate.Hour, 72 + MyDate.Hour);
                int m1 = h1 == MyDate.Hour ? rnd.Next(MyDate.Minute, 60) : rnd.Next(0, 60);
                DateTime date = new DateTime(MyDate.Year, MyDate.Month, MyDate.Day + h1 / 24, h1 % 24, m1, 0); //дата и время отправления
                TimeSpan time = new TimeSpan(h1 % 24, m1, 0); //время отправления
                int h2 = rnd.Next(MyDate.Hour, h1);
                int m2;
                if (h2 == h1)
                {
                    if (h2 == MyDate.Hour)
                        m2 = rnd.Next(MyDate.Minute, date.Minute);
                    else
                        m2 = rnd.Next(0, date.Minute);
                }
                else
                {
                    if (h2 == MyDate.Hour)
                        m2 = rnd.Next(MyDate.Minute, 60);
                    else
                        m2 = rnd.Next(0, 60);
                }
                DateTime dateLife = new DateTime(MyDate.Year, MyDate.Month, MyDate.Day + h2 / 24, h2 % 24, m2, 0); //время жизни рейса

                //получение айди типов перевозимого "груза" (грузовой или пассажирский)
                SQLiteCommand cargoType = new SQLiteCommand
                {
                    Connection = sConn,
                    CommandText = "select id from TypesOfCargo"
                };
                reader = cargoType.ExecuteReader();
                List<int> cargoTypes = new List<int>();
                while (reader.Read())
                    cargoTypes.Add(Convert.ToInt32(reader[0]));

                //получение типов направления (прямой или прямой-обратный)
                SQLiteCommand directionType = new SQLiteCommand
                {
                    Connection = sConn,
                    CommandText = "select id from DirectionTypes"
                };
                reader = directionType.ExecuteReader();
                List<int> directionTypes = new List<int>();
                while (reader.Read())
                    directionTypes.Add(Convert.ToInt32(reader[0]));

                int cargo = cargoTypes[rnd.Next(2)]; //тип перевозимого "груза"
                int typeFlight = typeFlightsIDs[rnd.Next(2)]; //регулярный или чартерный
                int direction;
                if (typeFlight == 2)
                    direction = 2;
                else
                    direction = directionTypes[rnd.Next(2)]; //тип направления
                int income; //доход от рейса
                int route = idRoute[rnd.Next(c)];

                if (cargo == 1) //если рейс грузовой, то цена фиксированная
                    income = rnd.Next(5000, 15001);

                else if (time >= new TimeSpan(0, 0, 0) && time < new TimeSpan(10, 0, 0)) //с 0 до 10 часов
                    income = rnd.Next(10000, 15001);
                else if (time >= new TimeSpan(10, 0, 0) && time < new TimeSpan(14, 0, 0)) //с 10 до 14 часов
                    income = rnd.Next(8000, 13001);
                else if (time >= new TimeSpan(14, 0, 0) && time < new TimeSpan(20, 0, 0)) //с 14 до 20 часов
                    income = rnd.Next(5000, 10001);
                else
                    income = rnd.Next(10000, 15001);

                SQLiteCommand minutesCount = new SQLiteCommand
                {
                    Connection = sConn,
                    CommandText = @"select minutes from directions where id=@route"
                };
                minutesCount.Parameters.AddWithValue("@route", route);

                //вставка рейса в БД
                SQLiteCommand insert = new SQLiteCommand
                {
                    Connection = sConn,
                    CommandText = @"INSERT INTO Flights (id_type,id_route,date,lifetime,id_typeofcargo,income,id_directiontype,finishTime) VALUES (@idType,@IDRoute,@date,@lifetime,@IDTypeOfCargo,@Income,@IDDirectionType,@finishTime)",
                };
                if(direction == 2)
                {
                    insert.Parameters.AddWithValue(@"Income", 2*income);
                    insert.Parameters.AddWithValue("@finishTime", date.AddMinutes(2*(int)minutesCount.ExecuteScalar()));
                }
                else
                {
                    insert.Parameters.AddWithValue(@"Income", income);
                    insert.Parameters.AddWithValue("@finishTime", date.AddMinutes((int)minutesCount.ExecuteScalar()));
                }
                insert.Parameters.AddWithValue("@idType", typeFlight);
                insert.Parameters.AddWithValue("@IDRoute", route);
                insert.Parameters.AddWithValue("@date", date);
                insert.Parameters.AddWithValue("@lifetime", dateLife);
                insert.Parameters.AddWithValue(@"IDTypeOfCargo", cargo);
                insert.Parameters.AddWithValue(@"IDDirectionType", direction);
                insert.ExecuteNonQuery();
            }
        }

        // Выводит количество рейсов которые можно взять
        public int GetCountFlight()
        {
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select count (*) FROM Flights where Flights.lifetime is not NULL",
            };
            return int.Parse(sCommand.ExecuteScalar().ToString());
        }

        // Выводит массив строк, содержащих данные о рейсах, которые можно взять
        public string[,] GetFlight()
        {
            int N = 9;
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select C.Name, CC.Name, TypeFlight.Name, Date, Flights.id,Flights.lifetime,TypesOfCargo.type,Flights.income,DirectionTypes.type  FROM Flights, Cities AS C, Cities AS CC
                                join Directions
                                on Flights.id_route = Directions.id 
                                join TypeFlight
                                on Flights.id_type = TypeFlight.id
                                join TypesOfCargo 
                                on TypesOfCargo.id = Flights.id_typeOfCargo
                                join DirectionTypes 
                                on DirectionTypes.id = Flights.id_directionType
                                where Directions.idCityBeg = C.id And Directions.idCityFin = CC.id AND Flights.lifetime is not NULL",
            };
            SQLiteDataReader reader = sCommand.ExecuteReader();
            string[,] s = new string[N, GetCountFlight()];
            int i = 0;
            while (reader.Read())
            {
                for (int j = 0; j < N; j++)
                    s[j, i] = reader[j].ToString();
                i++;
            }
            return s;
        }

        // Выводит количество взятых рейсов
        public int GetCountMyFlight()
        {
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select count(*) FROM Timetable",
            };
            return int.Parse(sCommand.ExecuteScalar().ToString());
        }

        // Выводит массив строк, содержащих данные о взятых рейсах
        public string[,] GetMyFlight()
        {
            int N = 11;
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select distinct C.Name, CC.Name, TypeFlight.Name, Date, Flights.id,Timetable.id_myplane,TypesOfCargo.type,Flights.income,DirectionTypes.type,typeplane.Name,flights.finishtime FROM Flights, Cities AS C, Cities AS CC
                                join Directions
                                on Flights.id_route = Directions.id 
                                join TypeFlight
                                on Flights.id_type = TypeFlight.id
                                join Timetable
                                on Timetable.id_flight = Flights.id
                                join TypesOfCargo 
                                on TypesOfCargo.id = Flights.id_typeOfCargo
                                join DirectionTypes 
                                on DirectionTypes.id = Flights.id_directionType
                                left join MyPLanes
                                on MyPLanes.id = TimeTable.id_myplane
                                left join typeplane
                                on myplanes.idType = typeplane.id
                                where Directions.idCityBeg = C.id And Directions.idCityFin = CC.id",
            };
            SQLiteDataReader reader = sCommand.ExecuteReader();
            string[,] s = new string[N, GetCountMyFlight()];
            int i = 0;
            while (reader.Read())
            {
                for (int j = 0; j < N; j++)
                    s[j, i] = reader[j].ToString();
                i++;
            }
            return s;
        }

        // выводит массив строк самолетов (всех)
        public string[,] GetPlanes()
        {
            int N = 8;
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT * FROM Planes",
            };
            SQLiteDataReader reader = sCommand.ExecuteReader();
            string[,] s = new string[N, GetCountPlanes()];
            int i = 0;
            while (reader.Read())
            {
                for (int j = 0; j < N; j++)
                    s[j, i] = reader[j].ToString();
                i++;
            }
            return s;
        }

        //выводит массив строк купленных самолетов
        public string[,] GetMyPlanes()
        {
            int N = 11;
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT Myplanes.id, planes.Name, cities.Name, planes.Cost, planes.Carrying, planes.PasCapacity, planes.Distance, planes.ArendCost, planes.[Expense (per km)], mypurchases.DateBegin, mypurchases.DateFinish
                                FROM MyPlanes join Planes on Myplanes.id_Plane=Planes.id
                                join MyPurchases on MyPlanes.id=MyPurchases.id_myplane left join Cities on MyPlanes.idPlace  = Cities.id"
            };
            SQLiteDataReader reader = comm.ExecuteReader();
            string[,] s = new string[N, GetCountMyPlanes()];
            int i = 0;
            while (reader.Read())
            {
                for (int j = 0; j < N; j++)
                    s[j, i] = reader[j].ToString();
                i++;
            }
            return s;
        }

        public string[,] GetMyHangarPlanes()
        {
            int N = 6;
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT  Myplanes.id, planes.Name, planes.Carrying, planes.PasCapacity, planes.Distance, planes.[Expense (per km)]
                FROM MyPlanes join Planes 
                on Myplanes.id_Plane=Planes.id 
                left join TimeTable
                on TimeTable.id_myplane = MyPlanes.id
                where MyPlanes.idType = 1 AND TimeTable.id_myplane is NULL"
            };
            SQLiteDataReader reader = comm.ExecuteReader();
            string[,] s = new string[N, GetCountMyPlanes()];
            int i = 0;
            while (reader.Read())
            {
                for (int j = 0; j < N; j++)
                    s[j, i] = reader[j].ToString();
                i++;
            }
            return s;
        } // выводит массив строк моих ангарных самолетов

        public int GetCountMyHangarPlanes()
        {
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT  Count(*)
                FROM MyPlanes join Planes 
                on Myplanes.id_Plane=Planes.id 
                left join TimeTable
                on TimeTable.id_myplane = MyPlanes.id
                where MyPlanes.idType = 1 AND TimeTable.id_myplane is NULL"
            };
            return int.Parse(sCommand.ExecuteScalar().ToString());
        } // выводит массив строк моих ангарных самолетов

        public string[,] GetMyPlanesInWindow()
        {
            int N = 4;
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT Myplanes.id, planes.Name, planes.Distance, timetable.id_flight
                                FROM MyPlanes join Planes on Myplanes.id_Plane=Planes.id left join timeTable on timetable.id_myplane = MyPlanes.id"
            };
            SQLiteDataReader reader = comm.ExecuteReader();
            string[,] s = new string[N, GetCountMyPlanes()];
            int i = 0;
            while (reader.Read())
            {
                for (int j = 0; j < N; j++)
                    s[j, i] = reader[j].ToString();
                i++;
            }
            return s;
        }

        // выводит количество моих самолетов
        public int GetCountMyPlanes()
        {
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select count(*) FROM MyPlanes",
            };
            return int.Parse(sCommand.ExecuteScalar().ToString());
        }

        // выводит количество самолетов в магазине
        public int GetCountPlanes()
        {
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select count(*) FROM Planes",
            };
            return int.Parse(sCommand.ExecuteScalar().ToString());
        }

        // покупка самолета по id
        public void BuyPlane(int idPlane)
        {
            SQLiteCommand sCommand = new SQLiteCommand // запрос Добавление самолета
            {
                Connection = sConn,
                CommandText = @"INSERT INTO MyPlanes (id,id_plane,idplace,idType) VALUES (@id,@idplane,1,1) "
            };
            Guid idG = Guid.NewGuid();
            string id = idG.ToString();
            sCommand.Parameters.AddWithValue("@idplane", idPlane);
            sCommand.Parameters.AddWithValue("@id", id);
            sCommand.ExecuteNonQuery();
            sCommand = new SQLiteCommand // запрос в бд стоимости самолета и названия
            {
                Connection = sConn,
                CommandText = @"select Cost from Planes where id = @idplane"
            };
            sCommand.Parameters.AddWithValue("@idplane", idPlane);
            SQLiteDataReader reader = sCommand.ExecuteReader();
            reader.Read();
            int cost = int.Parse(reader[0].ToString());
            GetMoney = GetMoney - cost;
            UpdateMoney(GetMoney);
            sCommand = new SQLiteCommand // запрос в бд обновление купленных предметов
            {
                Connection = sConn,
                CommandText = @"INSERT INTO MyPurchases (id_myplane,id_purchanse,DateBegin) VALUES (@id,3,@Date) "
            };
            sCommand.Parameters.AddWithValue("@id", id);
            sCommand.Parameters.AddWithValue("@Date", GetTime);
            sCommand.ExecuteNonQuery();
        }

        // аренда самолета по id
        public void ArendPlane(int idPlane, int months)
        {
            SQLiteCommand sCommand = new SQLiteCommand // запрос Добавление самолета
            {
                Connection = sConn,
                CommandText = @"INSERT INTO MyPlanes (id,id_plane, idPlace, idType) VALUES (@id,@idplane,1,1) "
            };
            Guid idG = Guid.NewGuid();
            string id = idG.ToString();
            sCommand.Parameters.AddWithValue("@idplane", idPlane);
            sCommand.Parameters.AddWithValue("@id", id);
            sCommand.ExecuteNonQuery();
            sCommand = new SQLiteCommand // запрос в бд стоимости аренды самолета и названия
            {
                Connection = sConn,
                CommandText = @"select ArendCost from Planes where id = @idplane"
            };
            sCommand.Parameters.AddWithValue("@idplane", idPlane);
            SQLiteDataReader reader = sCommand.ExecuteReader();
            reader.Read();
            int arcost = int.Parse(reader[0].ToString());
            GetMoney = GetMoney - arcost;
            UpdateMoney(GetMoney);
            sCommand = new SQLiteCommand // запрос в бд обновление купленных предметов
            {
                Connection = sConn,
                CommandText = @"INSERT INTO MyPurchases (id_myplane,id_purchanse,DateBegin,DateFinish,LastDatePay) VALUES (@id,1,@Date,@DateEnd,@DatePay) "
            };
            sCommand.Parameters.AddWithValue("@id", id);
            sCommand.Parameters.AddWithValue("@Date", GetTime);
            sCommand.Parameters.AddWithValue("@DateEnd", GetTime.AddMonths(months));
            sCommand.Parameters.AddWithValue("@DatePay", GetTime.AddMonths(1));
            sCommand.ExecuteNonQuery();
        }

        public void DeleteMyPlaneAfterArend(string idMyPlane)
        {
            SQLiteCommand sCommand = new SQLiteCommand // запрос удаления самолета
            {
                Connection = sConn,
                CommandText = @"DELETE FROM MyPurchases WHERE id_myplane = @idMyPlane;DELETE FROM MyPlanes WHERE id = @idMyPlane"
            };
            sCommand.Parameters.AddWithValue("@idMyPlane", idMyPlane);
            sCommand.ExecuteNonQuery();
        } // удаление самолета по айди

        public void DeleteMyPlane(string idMyPlane, int cost)
        {
            SQLiteCommand sCommand = new SQLiteCommand // запрос Добавление самолета
            {
                Connection = sConn,
                CommandText = @"DELETE FROM MyPurchases WHERE id_myplane = @idMyPlane;DELETE FROM MyPlanes WHERE id = @idMyPlane"
            };
            sCommand.Parameters.AddWithValue("@idMyPlane", idMyPlane);
            sCommand.ExecuteNonQuery();
            GetMoney = GetMoney + cost;
            UpdateMoney(GetMoney);
        }

        // новая игра
        public void NewGame()
        {
            SQLiteCommand sCommand = new SQLiteCommand // запрос удаление всех самолетов
            {
                Connection = sConn,
                CommandText = @"DELETE FROM MyPurchases; DELETE FROM MyPlanes;DELETE FROM TimeTable;DELETE FROM flights "
            };
            sCommand.ExecuteNonQuery();
            sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"update user set money = @Money, Date = @Date"
            };
            GetMoney = FirstMoney;
            sCommand.Parameters.AddWithValue("@Money", GetMoney);
            sCommand.Parameters.AddWithValue("@Date", DateTime.Now);
            sCommand.ExecuteNonQuery();
            NewFlight(15);
        }

        // добавление нашего рейса
        public void AddMyFlight(int idFlight)
        {
            SQLiteCommand sCommand = new SQLiteCommand // запрос Добавление рейса
            {
                Connection = sConn,
                CommandText = @"INSERT INTO Timetable (id_flight) VALUES (@idflight) "
            };
            sCommand.Parameters.AddWithValue("@idflight", idFlight);
            sCommand.ExecuteNonQuery();
            EditFlight(idFlight);
        }

        // обновления рейса
        public void EditFlight(int idFlight)
        {
            SQLiteCommand sCommand = new SQLiteCommand // запрос обновление рейсов
            {
                Connection = sConn,
                CommandText = @"update flights set lifetime = NULL where flights.id = @idFlight"
            };
            sCommand.Parameters.AddWithValue("@idflight", idFlight);
            sCommand.ExecuteNonQuery();
        }

        // выводит массив строк моих самолетов
        public string[] GetExpenseAndCity(string id)
        {
            int N = 3;
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT planes.[Expense (per km)],cities.Name,Cities.id
                FROM MyPlanes join Planes on Myplanes.id_Plane=Planes.id
                    left join Cities on Cities.id = MyPlanes.idPlace
                 where Myplanes.id = @idMyPlane"
            };
            comm.Parameters.AddWithValue("idMyPlane", id);
            SQLiteDataReader reader = comm.ExecuteReader();
            reader.Read();
            string[] s = new string[N];
            s[0] = reader[0].ToString();
            s[1] = reader[1].ToString();
            s[2] = reader[2].ToString();
            return s;
        }

        // выводит количество городов
        public int GetCountofCities()
        {
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select count (*) FROM Cities",
            };
            return int.Parse(sCommand.ExecuteScalar().ToString());
        }

        // выводит массив городов
        public string[,] GetCities()
        {
            int N = 2;
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT * from Cities"
            };
            SQLiteDataReader reader = comm.ExecuteReader();
            string[,] s = new string[N, GetCountofCities()];
            int i = 0;
            while (reader.Read())
            {
                for (int j = 0; j < N; j++)
                    s[j, i] = reader[j].ToString();
                i++;
            }
            return s;
        }

        // выводит дистанцию между самолетом
        public int GetDistance(string id, int IDPlace)
        {
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT Directions.Distance from MyPlanes join Directions 
                                on Directions.idCityBeg = MyPLanes.idPlace
                                join Cities on Cities.id = MyPlanes.idPlace
                                where( Directions.idCityBeg = MyPlanes.idPlace AND Directions.idCityFin = @idCity) OR (Directions.idCityBeg = @idCity AND Directions.idCityFin = MyPlanes.idPlace) 
                                AND MyPlanes.id = @idMyPlane"
            };
            sCommand.Parameters.AddWithValue("@idCity", IDPlace);
            sCommand.Parameters.AddWithValue("@idMyPlane", id);
            return int.Parse(sCommand.ExecuteScalar().ToString());
        }

        public int GetTimeGoTo(string id, int IDPlace)
        {
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT Directions.Minutes from MyPlanes join Directions 
        on Directions.idCityBeg = MyPLanes.idPlace
            join Cities on Cities.id = MyPlanes.idPlace
            where( Directions.idCityBeg = MyPlanes.idPlace AND Directions.idCityFin = @idCity) OR (Directions.idCityBeg = @idCity AND Directions.idCityFin = MyPlanes.idPlace) 
            AND MyPlanes.id = @idMyPlane"
            };
            sCommand.Parameters.AddWithValue("@idCity", IDPlace);
            sCommand.Parameters.AddWithValue("@idMyPlane", id);
            int res = (int)sCommand.ExecuteScalar();
            return int.Parse(sCommand.ExecuteScalar().ToString());
        } // выводит время между самолетом

        public string[,] GetCitiesFromTo(int idFirst, string idPlane)
        {
            int N = 2;
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT Cities.id, Cities.Name from Directions join Cities on Directions.idCityFin = Cities.id
        join MyPlanes on MyPlanes.idPlace = @idCity  join Planes on Planes.id = MyPlanes.id_plane
            where(Directions.idCityBeg = @idCity) AND Directions.Distance <= PLanes.Distance and MyPlanes.id = @idplane"
            };
            sCommand.Parameters.AddWithValue("@idCity", idFirst);
            sCommand.Parameters.AddWithValue("@idplane", idPlane);
            SQLiteDataReader reader = sCommand.ExecuteReader();
            string[,] s = new string[N, GetCountCitiesFromTo(idFirst, idPlane)];
            int i = 0;
            while (reader.Read())
            {
                for (int j = 0; j < N; j++)
                    s[j, i] = reader[j].ToString();
                i++;
            }
            return s;
        }

        // выводит дистанцию между самолетом
        public int GetCountCitiesFromTo(int idFirst, string idPlane)
        {
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT count(*) from Directions join Cities on Directions.idCityFin = Cities.id
        join MyPlanes on MyPLanes.idPlace = @idCity  join Planes on Planes.id = MyPLanes.id_plane
            where(Directions.idCityBeg = @idCity) AND Directions.Distance <= PLanes.Distance and MyPlanes.id = @idplane"
            };
            sCommand.Parameters.AddWithValue("@idCity", idFirst);
            sCommand.Parameters.AddWithValue("@idplane", idPlane);
            return int.Parse(sCommand.ExecuteScalar().ToString());
        } // выводит дистанцию между самолетом

        public void AddFlightsPlane(string NameFirst, int isSecond, int Cost, string idPlane)
        {
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select Directions.id from Directions join Cities on Cities.id = Directions.idCityBeg where (Cities.Name = @first and Directions.idCityFin= @second) OR  (Directions.idCityBeg = @second and Cities.Name= @first)"
            };
            sCommand.Parameters.AddWithValue("@first", NameFirst);
            sCommand.Parameters.AddWithValue("@second", isSecond);
            int idRoute = int.Parse(sCommand.ExecuteScalar().ToString());
            SQLiteCommand minutesCount = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select minutes from directions where id=@route"
            };
            minutesCount.Parameters.AddWithValue("@route", idRoute);
            sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"INSERT INTO Flights (id_type,id_route,date,lifetime,id_typeofcargo,income,id_directiontype,finishTime) 
                    VALUES (@idType,@IDRoute,@date,@lifetime,@IDTypeOfCargo,@Income,@IDDirectionType,@finishTime)"
            };
            sCommand.Parameters.AddWithValue("@idType", 1);
            sCommand.Parameters.AddWithValue("@IDRoute", idRoute);
            sCommand.Parameters.AddWithValue("@date", GetTime);
            sCommand.Parameters.AddWithValue("@lifetime", null);
            sCommand.Parameters.AddWithValue("@IDTypeOfCargo", 2);
            sCommand.Parameters.AddWithValue("@Income", Cost);
            sCommand.Parameters.AddWithValue("@IDDirectionType", 1);
            sCommand.Parameters.AddWithValue("@finishTime", GetTime.AddMinutes((int)minutesCount.ExecuteScalar()));
            sCommand.ExecuteNonQuery();
            sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select last_insert_rowid()"
            };
            int idFlight = int.Parse(sCommand.ExecuteScalar().ToString());
            sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"insert into Timetable (id_myplane,id_flight) values (@idMyPlane, @idMyflight) "
            };
            sCommand.Parameters.AddWithValue("@idMyPlane", idPlane);
            sCommand.Parameters.AddWithValue("@idMyflight", idFlight);
            sCommand.ExecuteNonQuery();
            sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"update MyPlanes set idType = 2 where MyPlanes.id = @idMyPlane"
            };
            sCommand.Parameters.AddWithValue("@idMyPlane", idPlane);
            sCommand.ExecuteNonQuery();
        } //осуществляет перегон самолета

        public int GetCountMyArendPlanes()
        {
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT count (*) 
FROM MyPlanes join Planes on Myplanes.id_Plane=Planes.id 
join MyPurchases on MyPlanes.id=MyPurchases.id_myplane
where MyPurchases.id_purchanse = 1"
            };
            return int.Parse(comm.ExecuteScalar().ToString());
        }//количество арендованных самолетов

        public int GetCountMyLeasingPlanes()
        {
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT count (*) 
FROM MyPlanes join Planes on Myplanes.id_Plane=Planes.id 
join MyPurchases on MyPlanes.id=MyPurchases.id_myplane
where MyPurchases.id_purchanse = 2"
            };
            return int.Parse(comm.ExecuteScalar().ToString());
        }//количество арендованных в лизинге

        //добавление самолета в рейс
        public void AddPlainInFlight(int flightID, string planeID)
        {
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"update TimeTable set id_myplane=@IDplane where id_flight=@IDflight"
            };
            comm.Parameters.AddWithValue("@IDplane", planeID);
            comm.Parameters.AddWithValue("@idflight", flightID);
            comm.ExecuteNonQuery();
        }

        public string[,] GetMyArendPlanes()
        {
            int N = 3;
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT Myplanes.id, mypurchases.LastDatePay , mypurchases.DateFinish
FROM MyPlanes join Planes on Myplanes.id_Plane=Planes.id 
join MyPurchases on MyPlanes.id=MyPurchases.id_myplane
where MyPurchases.id_purchanse = 1"
            };
            SQLiteDataReader reader = comm.ExecuteReader();
            string[,] s = new string[N, GetCountMyArendPlanes()];
            int i = 0;
            while (reader.Read())
            {
                for (int j = 0; j < N; j++)
                    s[j, i] = reader[j].ToString();
                i++;
            }
            return s;
        }//список арендованных самолетов

        public string[,] GetMyLeasingPlanes()
        {
            int N = 4;
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"SELECT Myplanes.id, mypurchases.LastDatePay , mypurchases.DateFinish, mypurchases.id_purchanse
FROM MyPlanes join Planes on Myplanes.id_Plane=Planes.id 
join MyPurchases on MyPlanes.id=MyPurchases.id_myplane
where MyPurchases.id_purchanse = 2"
            };
            SQLiteDataReader reader = comm.ExecuteReader();
            string[,] s = new string[N, GetCountMyLeasingPlanes()];
            int i = 0;
            while (reader.Read())
            {
                for (int j = 0; j < N; j++)
                    s[j, i] = reader[j].ToString();
                i++;
            }
            return s;
        }//список самолетов в лизинг

        public void ArendPay()
        {
            int n = GetCountMyArendPlanes();
            string[,] s = GetMyArendPlanes();
            string NamePlane = "";
            DateTime LastArend, nowtime;
            nowtime = GetTime;
            for (int i = 0; i < n; i++)
            {
                LastArend = DateTime.Parse(s[1, i]);
                if (nowtime >= DateTime.Parse(s[2, i]))
                {
                    DeleteMyPlaneAfterArend(s[0, i]);
                }
                else
                {
                    if (LastArend <= nowtime)
                    {
                        SQLiteCommand comm = new SQLiteCommand
                        {
                            Connection = sConn,
                            CommandText = @"SELECT PLanes.Name,Planes.ArendCost 
from Myplanes join Planes on Myplanes.id_plane=Planes.id where Myplanes.id=@idPlane"
                        };
                        comm.Parameters.AddWithValue("@idPlane", s[0, i]);
                        SQLiteDataReader reader = comm.ExecuteReader();
                        reader.Read();
                        BigInteger arcost = BigInteger.Parse(reader[1].ToString());
                        NamePlane = reader[0].ToString();
                        if (GetMoney >= arcost)
                        {
                            LastArend = LastArend.AddMonths(1);
                            SQLiteCommand sCommand = new SQLiteCommand // запрос в бд обновление денег
                            {
                                Connection = sConn,
                                CommandText = @"update user set money = @Money"
                            };
                            sCommand.Parameters.AddWithValue("@Money", GetMoney - arcost);
                            sCommand.ExecuteNonQuery();
                            sCommand = new SQLiteCommand // запрос в бд обновление денег
                            {
                                Connection = sConn,
                                CommandText = @"update MyPurchases set LastDatePay = @LastArend where  MyPurchases.id_myplane = @idPlane"
                            };
                            sCommand.Parameters.AddWithValue("@LastArend", LastArend);
                            sCommand.Parameters.AddWithValue("@idPlane", s[0, i]);
                            sCommand.ExecuteNonQuery();
                        }
                        else
                        {
                            DeleteMyPlaneAfterArend(s[0, i]);
                            MessageBox.Show("Не хватает денег для списания арендной платы. " + NamePlane + " будет изъят.");
                        }
                    }
                }
            }
        }

        // Удаление нашего рейса по id
        public void DeleteMyFlight(int id)
        {
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"DELETE FROM TimeTable WHERE id_flight = @ID"
            };
            sCommand.Parameters.AddWithValue("@ID", id);
            sCommand.ExecuteNonQuery();
        }

        //расстояние между городами по названию городов
        public int DistanceBetweenCities(string from, string to)
        {
            int fromID, toID;
            //получение ID первого города
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select id from cities where name=@from"
            };
            comm.Parameters.AddWithValue("@from", from);
            fromID = (int)comm.ExecuteScalar();
            //получение ID второго города
            comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select id from cities where name=@to"
            };
            comm.Parameters.AddWithValue("@to", to);
            toID = (int)comm.ExecuteScalar();
            comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = "select distance from directions where idcitybeg=@from and idcityfin=@to or idcitybeg=@to and idcityfin=@from"
            };
            comm.Parameters.AddWithValue("@from", fromID);
            comm.Parameters.AddWithValue("@to", toID);
            return (int)comm.ExecuteScalar();
        }

        //проверяет наличие id самолета в timetable
        public bool PlaneIsInTimetable(string id)
        {
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select count (*) from timetable where timetable.id_myplane=@id"
            };
            comm.Parameters.AddWithValue("@id", id);
            int n = Convert.ToInt32(comm.ExecuteScalar());
            if (n == 0) return false;
            return true;
        }

        //получить ID города по названию
        public int GetCityID(string city)
        {
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select id from cities where name=@city"
            };
            comm.Parameters.AddWithValue("@city", city);
            return (int)comm.ExecuteScalar();
        }

        //в каком городе находится самолет (возвращает ID города)
        public int PlaneCityId(string id)
        {
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select idPlace from myplanes where id=@id"
            };
            comm.Parameters.AddWithValue("@id", id);
            return (int)comm.ExecuteScalar();
        }

        //получить имя самолета из ID из таблицы myplanes
        public string GetPlaneName(string id)
        {
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select planes.name from planes
                                join myplanes
                                on myplanes.id_plane=planes.id
                                where myplanes.id=@id"
            };
            comm.Parameters.AddWithValue("@id", id);
            SQLiteDataReader reader = comm.ExecuteReader();
            reader.Read();
            return reader[0].ToString();
        }

        //смена статуса самолета на "в полете" по имени самолета
        public void ChangeStatus(int flightID)
        {
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select id_myplane from timetable where id_flight=@id"
            };
            comm.Parameters.AddWithValue("@id", flightID);
            SQLiteDataReader reader = comm.ExecuteReader();
            reader.Read();
            string planeID = reader[0].ToString();
            comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"update myplanes set idtype=2 where id=@idPlane"
            };
            comm.Parameters.AddWithValue("@idplane", planeID);
            comm.ExecuteNonQuery();
        }

        //обновление города у самолета, когда он прилетел
        public void UpdatePlace(int myflight, string city)
        {
            SQLiteCommand comm0 = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select id_myplane from timetable where id_flight=@myflight"
            };
            comm0.Parameters.AddWithValue("@myflight", myflight);
            SQLiteCommand comm = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select id from cities where name=@name"
            };
            comm.Parameters.AddWithValue("@name", city);
            SQLiteCommand comm1 = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"update myplanes set idplace=@place,idtype=1 where id=@idPlane"
            };
            comm1.Parameters.AddWithValue("@place", (int)comm.ExecuteScalar());
            comm1.Parameters.AddWithValue("@idplane", comm0.ExecuteScalar());
            comm1.ExecuteNonQuery();
        }

        // лизинг самолета по id
        public void LeasingPlane(int idPlane, int months)
        {
            SQLiteCommand sCommand = new SQLiteCommand // запрос Добавление самолета
            {
                Connection = sConn,
                CommandText = @"INSERT INTO MyPlanes (id,id_plane, idPlace, idType) VALUES (@id,@idplane,1,2) "
            };
            Guid idG = Guid.NewGuid();
            string id = idG.ToString();
            sCommand.Parameters.AddWithValue("@idplane", idPlane);
            sCommand.Parameters.AddWithValue("@id", id);
            sCommand.ExecuteNonQuery();
            sCommand = new SQLiteCommand // запрос в бд стоимости аренды самолета и названия
            {
                Connection = sConn,
                CommandText = @"select Cost from Planes where id = @idplane"
            };
            sCommand.Parameters.AddWithValue("@idplane", idPlane);
            SQLiteDataReader reader = sCommand.ExecuteReader();
            reader.Read();
            BigInteger costleas = BigInteger.Parse(reader[0].ToString()) / months;
            GetMoney = GetMoney - costleas;
            UpdateMoney(GetMoney);
            sCommand = new SQLiteCommand // запрос в бд обновление купленных предметов
            {
                Connection = sConn,
                CommandText = @"INSERT INTO MyPurchases (id_myplane,id_purchanse,DateBegin,DateFinish,LastDatePay) VALUES (@id,2,@Date,@DateEnd,@DatePay) "
            };
            sCommand.Parameters.AddWithValue("@id", id);
            sCommand.Parameters.AddWithValue("@Date", GetTime);
            sCommand.Parameters.AddWithValue("@DateEnd", GetTime.AddMonths(months));
            sCommand.Parameters.AddWithValue("@DatePay", GetTime.AddMonths(1));
            sCommand.ExecuteNonQuery();
        }

        public void LeasingPay()
        {
            int n = GetCountMyLeasingPlanes();
            string[,] s = GetMyLeasingPlanes();
            string NamePlane = "", empty = "";
            DateTime LastArend, nowtime;
            nowtime = GetTime;
            for (int i = 0; i < n; i++)
            {
                LastArend = DateTime.Parse(s[1, i]);
                if (nowtime >= DateTime.Parse(s[2, i]) && s[2, i] != "")
                {
                    s[2, i] = "";
                    SQLiteCommand comm = new SQLiteCommand
                    {
                        Connection = sConn,
                        CommandText = @"SELECT PLanes.Name,Planes.Cost 
from Myplanes join Planes on Myplanes.id_plane=Planes.id where Myplanes.id=@idPlane"
                    };
                    comm.Parameters.AddWithValue("@idPlane", s[0, i]);
                    SQLiteDataReader reader = comm.ExecuteReader();
                    reader.Read();
                    BigInteger arcost = BigInteger.Parse(reader[1].ToString());
                    NamePlane = reader[0].ToString();
                    SQLiteCommand sCommand = new SQLiteCommand // запрос в бд обновление приобретений
                    {
                        Connection = sConn,
                        CommandText = @"update MyPurchases set id_purchanse = @purch, DateFinish = NULL,LastDatePay = NULL where  MyPurchases.id_myplane = @idPlane"
                    };
                    sCommand.Parameters.AddWithValue("@idPlane", s[0, i]);
                    sCommand.Parameters.AddWithValue("@purch", 3);
                    sCommand.ExecuteNonQuery();
                    MessageBox.Show("Самолет " + NamePlane + " теперь Ваш!");
                }
                else
                {
                    if (LastArend <= nowtime)
                    {
                        SQLiteCommand comm = new SQLiteCommand
                        {
                            Connection = sConn,
                            CommandText = @"SELECT PLanes.Name,Planes.Cost 
from Myplanes join Planes on Myplanes.id_plane=Planes.id where Myplanes.id=@idPlane"
                        };
                        comm.Parameters.AddWithValue("@idPlane", s[0, i]);
                        SQLiteDataReader reader = comm.ExecuteReader();
                        reader.Read();
                        BigInteger arcost = BigInteger.Parse(reader[1].ToString());
                        NamePlane = reader[0].ToString();
                        if (GetMoney >= arcost)
                        {
                            LastArend = LastArend.AddMonths(1);
                            SQLiteCommand sCommand = new SQLiteCommand // запрос в бд обновление денег
                            {
                                Connection = sConn,
                                CommandText = @"update user set money = @Money"
                            };
                            sCommand.Parameters.AddWithValue("@Money", GetMoney - arcost);
                            sCommand.ExecuteNonQuery();
                            sCommand = new SQLiteCommand // запрос в бд обновление приобретений
                            {
                                Connection = sConn,
                                CommandText = @"update MyPurchases set LastDatePay = @LastArend where  MyPurchases.id_myplane = @idPlane"
                            };
                            sCommand.Parameters.AddWithValue("@LastArend", LastArend);
                            sCommand.Parameters.AddWithValue("@idPlane", s[0, i]);
                            sCommand.ExecuteNonQuery();
                        }
                        else
                        {
                            DeleteMyPlaneAfterArend(s[0, i]);
                            MessageBox.Show("Не хватает денег для списания платы. " + NamePlane + " будет изъят.");
                        }
                    }
                }
            }
        }

        public int GetCountMyBuePlanes()
        {
            SQLiteCommand sCommand = new SQLiteCommand
            {
                Connection = sConn,
                CommandText = @"select count (*) FROM MyPlanes 
    join MyPurchases 
    on MyPurchases.id_myplane = MyPlanes.id
    where MyPurchases.id_purchanse = 3",
            };
            return int.Parse(sCommand.ExecuteScalar().ToString());
        }
    }
}