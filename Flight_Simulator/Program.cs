﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace Flight_Simulator
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (IsDateBase())
                Application.Run(new MainWindow());
            else
            {
                MessageBox.Show("Переместите базу данных (DataBase.sqlite) к exe файлу", "DataBase not found!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }

        static private bool IsDateBase()
        {
            return File.Exists(Directory.GetCurrentDirectory() + "\\DataBase.sqlite");
        }
    }
}