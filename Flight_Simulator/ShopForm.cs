﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Flight_Simulator
{
    public partial class ShopForm : Form
    {
        public Game Game;
        public int SecondsToAdd = 1;
        public ShopForm(Game MyGame)
        {
            InitializeComponent();
            Game = MyGame;
            lblMoney.Text = Game.GetMoneyStr();
            InitPlanes();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void ShopForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            e.Cancel = true;
        }

        private void InitPlanes()
        {
            lviPlanes.Items.Clear();
            int n = Game.GetCountPlanes();
            string[,] s = Game.GetPlanes();
            for (int i = 0; i < n; i++)
            {
                lviPlanes.Items.Add(new ListViewItem(new[] { s[1, i], s[2, i], s[3, i], s[4, i], s[5, i], s[6, i], s[7, i] },0) { Tag = s[0, i] });
                if (int.Parse(s[2, i]) <= Game.GetMoney)
                    lviPlanes.Items[i].BackColor = Color.Green;
                else
                    lviPlanes.Items[i].BackColor = Color.Gray;
            }
            lviPlanes.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lviPlanes.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void timerToTime_Tick(object sender, EventArgs e)
        {
            lblTime.Text = Game.GetTime.ToString();
        }

        private void btnBuy_Click(object sender, EventArgs e)
        {
            if (lviPlanes.SelectedItems.Count == 0) //если юзер не выбрал самолет
            {
                MessageBox.Show("Сначала выберите самолет!");
                return;
            }
            if (BigInteger.Parse(lviPlanes.SelectedItems[0].SubItems[1].Text) > Game.GetMoney) //если недостаточно денег для покупки выбранного самолета
            {
                MessageBox.Show(@"Недостаточно денег для покупки");
                return;
            }
            try
            {
                Game.BuyPlane(int.Parse(lviPlanes.SelectedItems[0].Tag.ToString()));
                MessageBox.Show(@"Вы приобрели самолет : " + lviPlanes.SelectedItems[0].SubItems[0].Text,"Новое приобретение!",MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
                lblMoney.Text = Game.GetMoneyStr();
                InitPlanes();
            }
            catch
            {
                MessageBox.Show(@"Ошибка с базой данных");
            }
        }

        private void ShopForm_Load(object sender, EventArgs e)
        {
            InitPlanes();
            lblMoney.Text = Game.GetMoneyStr();
            if(lviPlanes.Items.Count!=0)
                lviPlanes.Items[0].Selected = true;
        }

        private void lviPlanes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lviPlanes.SelectedItems.Count != 0)
            {
                lblCostArenda.Text = (BigInteger.Parse(lviPlanes.SelectedItems[0].SubItems[5].Text) * BigInteger.Parse(numUPDArenda.Value.ToString())).ToString();
                lblCostLizing.Text = (BigInteger.Parse(lviPlanes.SelectedItems[0].SubItems[1].Text)/ int.Parse(numUPDLizing.Value.ToString())).ToString();
                lblCost.Text = lviPlanes.SelectedItems[0].SubItems[1].Text;
                lblName.Text = lviPlanes.SelectedItems[0].SubItems[0].Text;
            }
        }

        private void numUPDArenda_ValueChanged(object sender, EventArgs e)
        {
            if (lviPlanes.SelectedItems.Count != 0)
            {
                lblCostArenda.Text = (BigInteger.Parse(lviPlanes.SelectedItems[0].SubItems[5].Text) * BigInteger.Parse(numUPDArenda.Value.ToString())).ToString();
            }
        }

        private void btnRent_Click(object sender, EventArgs e)
        {
            int months = int.Parse(numUPDArenda.Value.ToString());
            if (numUPDArenda.Value <= 60 && numUPDArenda.Value > 0)
            {
                if (lviPlanes.SelectedItems.Count == 0) //если юзер не выбрал самолет
                {
                    MessageBox.Show("Сначала выберите самолет!");
                    return;
                }
                if (BigInteger.Parse(lviPlanes.SelectedItems[0].SubItems[5].Text) > Game.GetMoney) //если недостаточно денег для покупки выбранного самолета
                {
                    MessageBox.Show(@"Недостаточно денег для аренды");
                    return;
                }
                try
                {
                    Game.ArendPlane(int.Parse(lviPlanes.SelectedItems[0].Tag.ToString()), months);
                    MessageBox.Show(@"Вы приобрели самолет : " + lviPlanes.SelectedItems[0].SubItems[0].Text + " на " + numUPDArenda.Value + " месяцев.");
                    lblMoney.Text = Game.GetMoneyStr();
                }
                catch
                {
                    MessageBox.Show(@"Ошибка с базой данных");
                }
            }
            else
            {
                MessageBox.Show("Введите количество месяцев");
                numUPDArenda.Focus();
            }
        }

        private void btnLeasing_Click(object sender, EventArgs e)
        {
            int months = int.Parse(numUPDLizing.Value.ToString());
            BigInteger cost = BigInteger.Parse(lviPlanes.SelectedItems[0].SubItems[1].Text);
            BigInteger costLeas = cost / months;
            if (numUPDLizing.Value <= 100 && numUPDLizing.Value > 0)
            {
                if (lviPlanes.SelectedItems.Count == 0) //если юзер не выбрал самолет
                {
                    MessageBox.Show("Сначала выберите самолет!");
                    return;
                }
                if (costLeas > Game.GetMoney) //если недостаточно денег для покупки выбранного самолета
                {
                    MessageBox.Show(@"Недостаточно денег для лизинга");
                    return;
                }
                try
                {
                    Game.LeasingPlane(int.Parse(lviPlanes.SelectedItems[0].Tag.ToString()), months);
                    MessageBox.Show(@"Вы приобрели самолет : " + lviPlanes.SelectedItems[0].SubItems[0].Text + " в лизинг на " + numUPDLizing.Value + " месяцев.");
                    lblMoney.Text = Game.GetMoneyStr();
                }
                catch
                {
                    MessageBox.Show(@"Ошибка с базой данных");
                }
            }
            else
            {
                MessageBox.Show("Введите количество месяцев");
                numUPDLizing.Focus();
            }
        }

        private void numUPDLizing_ValueChanged(object sender, EventArgs e)
        {
            if (lviPlanes.SelectedItems.Count != 0)
            {
                lblCostLizing.Text = (BigInteger.Parse(lviPlanes.SelectedItems[0].SubItems[1].Text) / int.Parse(numUPDLizing.Value.ToString())).ToString();
            }
        }
    }
}