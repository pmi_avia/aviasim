﻿namespace Flight_Simulator
{
    partial class MainWindow
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblMoney = new System.Windows.Forms.Label();
            this.btnPlay = new System.Windows.Forms.Button();
            this.btnX60 = new System.Windows.Forms.Button();
            this.btnX3600 = new System.Windows.Forms.Button();
            this.lviAvailableFlights = new System.Windows.Forms.ListView();
            this.From = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.To = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Type = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Date = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TypeOfCargo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Income = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DirectionType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Distance = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextFlights = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.взятьРейсToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.пометитьРейсToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lviMyFlights = new System.Windows.Forms.ListView();
            this.MyFrom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MyTo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MyType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MyDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MyDateArrive = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MyTypeOfCargo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MyIncome = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MyTypeOfDestenation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MyPlane = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MyDistance = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MyInFlight = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMyFlights = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.назначитьСамолетToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отказатьсяОтРейсаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timerToTime = new System.Windows.Forms.Timer(this.components);
            this.GameTime = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSand = new System.Windows.Forms.Button();
            this.btnHangar = new System.Windows.Forms.Button();
            this.btnShop = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblTime = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.играToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новаяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.загрузитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.timerFlights = new System.Windows.Forms.Timer(this.components);
            this.btnX86400 = new System.Windows.Forms.Button();
            this.contextFlights.SuspendLayout();
            this.contextMyFlights.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMoney
            // 
            this.lblMoney.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMoney.ForeColor = System.Drawing.Color.Green;
            this.lblMoney.Location = new System.Drawing.Point(0, 0);
            this.lblMoney.Name = "lblMoney";
            this.lblMoney.Size = new System.Drawing.Size(125, 72);
            this.lblMoney.TabIndex = 3;
            this.lblMoney.Text = "lblMoney";
            this.lblMoney.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPlay
            // 
            this.btnPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPlay.Location = new System.Drawing.Point(3, 4);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(60, 42);
            this.btnPlay.TabIndex = 3;
            this.btnPlay.Text = "▶";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // btnX60
            // 
            this.btnX60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnX60.Location = new System.Drawing.Point(69, 4);
            this.btnX60.Name = "btnX60";
            this.btnX60.Size = new System.Drawing.Size(60, 42);
            this.btnX60.TabIndex = 4;
            this.btnX60.Text = "▶▶";
            this.btnX60.UseVisualStyleBackColor = true;
            this.btnX60.Click += new System.EventHandler(this.btnX60_Click);
            // 
            // btnX3600
            // 
            this.btnX3600.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnX3600.Location = new System.Drawing.Point(134, 4);
            this.btnX3600.Name = "btnX3600";
            this.btnX3600.Size = new System.Drawing.Size(60, 42);
            this.btnX3600.TabIndex = 6;
            this.btnX3600.Text = "▶▶▶";
            this.btnX3600.UseVisualStyleBackColor = true;
            this.btnX3600.Click += new System.EventHandler(this.btnX3600_Click);
            // 
            // lviAvailableFlights
            // 
            this.lviAvailableFlights.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.From,
            this.To,
            this.Type,
            this.Date,
            this.TypeOfCargo,
            this.Income,
            this.DirectionType,
            this.Distance});
            this.lviAvailableFlights.ContextMenuStrip = this.contextFlights;
            this.lviAvailableFlights.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lviAvailableFlights.FullRowSelect = true;
            this.lviAvailableFlights.GridLines = true;
            this.lviAvailableFlights.Location = new System.Drawing.Point(3, 16);
            this.lviAvailableFlights.MultiSelect = false;
            this.lviAvailableFlights.Name = "lviAvailableFlights";
            this.lviAvailableFlights.Size = new System.Drawing.Size(906, 237);
            this.lviAvailableFlights.TabIndex = 12;
            this.lviAvailableFlights.UseCompatibleStateImageBehavior = false;
            this.lviAvailableFlights.View = System.Windows.Forms.View.Details;
            // 
            // From
            // 
            this.From.Text = "Отправление";
            // 
            // To
            // 
            this.To.Text = "Прибытие";
            // 
            // Type
            // 
            this.Type.Text = "Регулярность рейса";
            // 
            // Date
            // 
            this.Date.Text = "Дата";
            // 
            // TypeOfCargo
            // 
            this.TypeOfCargo.Text = "Тип";
            // 
            // Income
            // 
            this.Income.Text = "Доход";
            // 
            // DirectionType
            // 
            this.DirectionType.Text = "Направление";
            // 
            // Distance
            // 
            this.Distance.Text = "Дистанция";
            // 
            // contextFlights
            // 
            this.contextFlights.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.взятьРейсToolStripMenuItem,
            this.toolStripMenuItem3,
            this.пометитьРейсToolStripMenuItem});
            this.contextFlights.Name = "contextMenuStrip2";
            this.contextFlights.Size = new System.Drawing.Size(158, 54);
            // 
            // взятьРейсToolStripMenuItem
            // 
            this.взятьРейсToolStripMenuItem.Name = "взятьРейсToolStripMenuItem";
            this.взятьРейсToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.взятьРейсToolStripMenuItem.Text = "Взять рейс";
            this.взятьРейсToolStripMenuItem.Click += new System.EventHandler(this.взятьРейсToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(154, 6);
            // 
            // пометитьРейсToolStripMenuItem
            // 
            this.пометитьРейсToolStripMenuItem.Name = "пометитьРейсToolStripMenuItem";
            this.пометитьРейсToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.пометитьРейсToolStripMenuItem.Text = "Пометить рейс";
            // 
            // lviMyFlights
            // 
            this.lviMyFlights.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.MyFrom,
            this.MyTo,
            this.MyType,
            this.MyDate,
            this.MyDateArrive,
            this.MyTypeOfCargo,
            this.MyIncome,
            this.MyTypeOfDestenation,
            this.MyPlane,
            this.MyDistance,
            this.MyInFlight});
            this.lviMyFlights.ContextMenuStrip = this.contextMyFlights;
            this.lviMyFlights.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lviMyFlights.FullRowSelect = true;
            this.lviMyFlights.GridLines = true;
            this.lviMyFlights.Location = new System.Drawing.Point(3, 16);
            this.lviMyFlights.MultiSelect = false;
            this.lviMyFlights.Name = "lviMyFlights";
            this.lviMyFlights.Size = new System.Drawing.Size(906, 303);
            this.lviMyFlights.TabIndex = 13;
            this.lviMyFlights.UseCompatibleStateImageBehavior = false;
            this.lviMyFlights.View = System.Windows.Forms.View.Details;
            // 
            // MyFrom
            // 
            this.MyFrom.Text = "Отправление";
            // 
            // MyTo
            // 
            this.MyTo.Text = "Прибытие";
            // 
            // MyType
            // 
            this.MyType.Text = "Регулярность";
            // 
            // MyDate
            // 
            this.MyDate.Text = "Дата отправления";
            // 
            // MyDateArrive
            // 
            this.MyDateArrive.Text = "Дата прибытия";
            // 
            // MyTypeOfCargo
            // 
            this.MyTypeOfCargo.Text = "Тип";
            // 
            // MyIncome
            // 
            this.MyIncome.Text = "Доход";
            // 
            // MyTypeOfDestenation
            // 
            this.MyTypeOfDestenation.Text = "Направление";
            // 
            // MyPlane
            // 
            this.MyPlane.Text = "Самолет";
            // 
            // MyDistance
            // 
            this.MyDistance.Text = "Дистанция";
            // 
            // MyInFlight
            // 
            this.MyInFlight.Text = "Статус самолета";
            // 
            // contextMyFlights
            // 
            this.contextMyFlights.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.назначитьСамолетToolStripMenuItem,
            this.отказатьсяОтРейсаToolStripMenuItem});
            this.contextMyFlights.Name = "contextMenuStrip1";
            this.contextMyFlights.Size = new System.Drawing.Size(185, 48);
            // 
            // назначитьСамолетToolStripMenuItem
            // 
            this.назначитьСамолетToolStripMenuItem.Name = "назначитьСамолетToolStripMenuItem";
            this.назначитьСамолетToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.назначитьСамолетToolStripMenuItem.Text = "Назначить самолет";
            this.назначитьСамолетToolStripMenuItem.Click += new System.EventHandler(this.назначитьСамолетToolStripMenuItem_Click);
            // 
            // отказатьсяОтРейсаToolStripMenuItem
            // 
            this.отказатьсяОтРейсаToolStripMenuItem.Name = "отказатьсяОтРейсаToolStripMenuItem";
            this.отказатьсяОтРейсаToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.отказатьсяОтРейсаToolStripMenuItem.Text = "Отказаться от рейса";
            this.отказатьсяОтРейсаToolStripMenuItem.Click += new System.EventHandler(this.отказатьсяОтРейсаToolStripMenuItem_Click);
            // 
            // timerToTime
            // 
            this.timerToTime.Enabled = true;
            this.timerToTime.Interval = 1000;
            this.timerToTime.Tick += new System.EventHandler(this.timerToTime_Tick);
            // 
            // GameTime
            // 
            this.GameTime.Enabled = true;
            this.GameTime.Interval = 1000;
            this.GameTime.Tick += new System.EventHandler(this.GameTime_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSand);
            this.panel1.Controls.Add(this.btnHangar);
            this.panel1.Controls.Add(this.btnShop);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(912, 100);
            this.panel1.TabIndex = 14;
            // 
            // btnSand
            // 
            this.btnSand.Location = new System.Drawing.Point(690, 39);
            this.btnSand.Name = "btnSand";
            this.btnSand.Size = new System.Drawing.Size(87, 47);
            this.btnSand.TabIndex = 23;
            this.btnSand.Text = "Отправка самолета в другой город";
            this.btnSand.UseVisualStyleBackColor = true;
            this.btnSand.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnHangar
            // 
            this.btnHangar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnHangar.Location = new System.Drawing.Point(605, 39);
            this.btnHangar.Name = "btnHangar";
            this.btnHangar.Size = new System.Drawing.Size(81, 47);
            this.btnHangar.TabIndex = 8;
            this.btnHangar.Text = "Ангар";
            this.btnHangar.UseVisualStyleBackColor = true;
            this.btnHangar.Click += new System.EventHandler(this.btnHangar_Click);
            // 
            // btnShop
            // 
            this.btnShop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnShop.Location = new System.Drawing.Point(518, 39);
            this.btnShop.Name = "btnShop";
            this.btnShop.Size = new System.Drawing.Size(81, 47);
            this.btnShop.TabIndex = 7;
            this.btnShop.Text = "Магазин";
            this.btnShop.UseVisualStyleBackColor = true;
            this.btnShop.Click += new System.EventHandler(this.btnShop_Click);
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.lblTime);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 24);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(138, 76);
            this.panel6.TabIndex = 21;
            // 
            // lblTime
            // 
            this.lblTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTime.Location = new System.Drawing.Point(0, 0);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(134, 72);
            this.lblTime.TabIndex = 5;
            this.lblTime.Text = "lblTime";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.btnX86400);
            this.panel2.Controls.Add(this.btnX3600);
            this.panel2.Controls.Add(this.btnX60);
            this.panel2.Controls.Add(this.btnPlay);
            this.panel2.Location = new System.Drawing.Point(144, 27);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(273, 53);
            this.panel2.TabIndex = 17;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.lblMoney);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(783, 24);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(129, 76);
            this.panel5.TabIndex = 20;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.играToolStripMenuItem,
            this.настройкиToolStripMenuItem1,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(912, 24);
            this.menuStrip1.TabIndex = 22;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // играToolStripMenuItem
            // 
            this.играToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новаяToolStripMenuItem,
            this.сохранитьToolStripMenuItem,
            this.загрузитьToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.играToolStripMenuItem.Name = "играToolStripMenuItem";
            this.играToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.играToolStripMenuItem.Text = "Игра";
            // 
            // новаяToolStripMenuItem
            // 
            this.новаяToolStripMenuItem.Name = "новаяToolStripMenuItem";
            this.новаяToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.новаяToolStripMenuItem.Text = "Новая";
            this.новаяToolStripMenuItem.Click += new System.EventHandler(this.новаяToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            // 
            // загрузитьToolStripMenuItem
            // 
            this.загрузитьToolStripMenuItem.Name = "загрузитьToolStripMenuItem";
            this.загрузитьToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.загрузитьToolStripMenuItem.Text = "Загрузить";
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            // 
            // настройкиToolStripMenuItem1
            // 
            this.настройкиToolStripMenuItem1.Name = "настройкиToolStripMenuItem1";
            this.настройкиToolStripMenuItem1.Size = new System.Drawing.Size(79, 20);
            this.настройкиToolStripMenuItem1.Text = "Настройки";
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lviAvailableFlights);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 100);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(912, 256);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Доступные рейсы";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lviMyFlights);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(0, 359);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(912, 322);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Выбранные рейсы";
            // 
            // timerFlights
            // 
            this.timerFlights.Enabled = true;
            this.timerFlights.Interval = 200;
            this.timerFlights.Tick += new System.EventHandler(this.timerFlights_Tick);
            // 
            // btnX86400
            // 
            this.btnX86400.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnX86400.Location = new System.Drawing.Point(200, 4);
            this.btnX86400.Name = "btnX86400";
            this.btnX86400.Size = new System.Drawing.Size(60, 42);
            this.btnX86400.TabIndex = 7;
            this.btnX86400.Text = "▶▶▶▶";
            this.btnX86400.UseVisualStyleBackColor = true;
            this.btnX86400.Click += new System.EventHandler(this.btnX86400_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 681);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Авиасимулятор";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.contextFlights.ResumeLayout(false);
            this.contextMyFlights.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblMoney;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button btnX10;
        private System.Windows.Forms.Button btnX60;
        private System.Windows.Forms.ListView lviAvailableFlights;
        private System.Windows.Forms.ColumnHeader From;
        private System.Windows.Forms.ColumnHeader To;
        private System.Windows.Forms.ColumnHeader Type;
        private System.Windows.Forms.ColumnHeader Date;
        private System.Windows.Forms.ListView lviMyFlights;
        private System.Windows.Forms.ColumnHeader MyFrom;
        private System.Windows.Forms.ColumnHeader MyTo;
        private System.Windows.Forms.ColumnHeader MyDate;
        private System.Windows.Forms.ColumnHeader MyType;
        private System.Windows.Forms.Timer timerToTime;
        private System.Windows.Forms.Timer GameTime;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Timer timerFlights;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem играToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem новаяToolStripMenuItem;
        private System.Windows.Forms.Button btnX3600;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem загрузитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.Button btnHangar;
        private System.Windows.Forms.Button btnShop;
        private System.Windows.Forms.ColumnHeader MyPlane;
        private System.Windows.Forms.ContextMenuStrip contextMyFlights;
        private System.Windows.Forms.ToolStripMenuItem назначитьСамолетToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отказатьсяОтРейсаToolStripMenuItem;
        private System.Windows.Forms.Button btnSand;
        private System.Windows.Forms.ColumnHeader TypeOfCargo;
        private System.Windows.Forms.ColumnHeader Income;
        private System.Windows.Forms.ColumnHeader DirectionType;
        private System.Windows.Forms.ColumnHeader MyTypeOfCargo;
        private System.Windows.Forms.ColumnHeader MyIncome;
        private System.Windows.Forms.ColumnHeader MyTypeOfDestenation;
        private System.Windows.Forms.ContextMenuStrip contextFlights;
        private System.Windows.Forms.ToolStripMenuItem взятьРейсToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem пометитьРейсToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader Distance;
        private System.Windows.Forms.ColumnHeader MyDistance;
        private System.Windows.Forms.ColumnHeader MyInFlight;
        private System.Windows.Forms.ColumnHeader MyDateArrive;
        private System.Windows.Forms.Button btnX86400;
    }
}

