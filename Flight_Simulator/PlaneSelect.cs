﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Flight_Simulator
{
    public partial class PlaneSelect : Form
    {
        private Game Game;
        public int flightID; //ID рейса, на который назначается самолет
        public int distance; //дистанция между городами
        public string from;

        public PlaneSelect()
        {
            InitializeComponent();
            Game = new Game();
        }

        //инициализация списка самолетов
        public void InitPlanes()
        {
            lviPlanes.Items.Clear();
            int n = Game.GetCountMyPlanes();
            string[,] s = Game.GetMyPlanesInWindow();
            for (int i = 0; i < n; i++)
            {
                if (Game.GetCityID(from) == Game.PlaneCityId(s[0, i])) //находится ли самолет в городе отправления
                    if (!Game.PlaneIsInTimetable(s[0, i])) //поставлен ли самолет на рейс
                        if (int.Parse(s[2, i]) > distance) //если дальность самолета больше расстояния между городами
                            if (s[3, i] != "")
                                lviPlanes.Items.Add(new ListViewItem(new[] {s[1, i], s[2, i], "Назначен на рейс"}, 0) {Tag = s[0, i]});
                            else
                                lviPlanes.Items.Add(new ListViewItem(new[] {s[1, i], s[2, i], "Свободен"}, 0) {Tag = s[0, i]});

            }
            lviPlanes.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lviPlanes.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        //загрузка формы
        private void PlaneSelect_Load(object sender, EventArgs e)
        {
            InitPlanes();
        }

        //добавить самолет в рейс
        private void btnAppointPlane_Click(object sender, EventArgs e)
        {
            if (lviPlanes.SelectedItems.Count != 0)
            {
                Game.AddPlainInFlight(flightID, lviPlanes.SelectedItems[0].Tag.ToString());
                MessageBox.Show(@"Самолет назначен");
                PlaneSelect pSelect = new PlaneSelect();
                pSelect.Close();
            }
            else
                MessageBox.Show(@"Выберете сначала самолет!");
        }
    }
}