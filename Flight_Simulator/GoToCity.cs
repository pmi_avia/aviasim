﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Flight_Simulator
{
    public partial class GoToCity : Form
    {
        public Game Game;
        public List<City> C;
        string[] ss;
        double COST = 0;

        public GoToCity(Game Mygame)
        {
            InitializeComponent();
            Game = Mygame;
            InitPlanes();
            C = new List<City>(0);
        }

        private void InitPlanes()
        {
            lviPlanes.Items.Clear();
            int n = Game.GetCountMyHangarPlanes();
            string[,] s = Game.GetMyHangarPlanes();
            for (int i = 0; i < n; i++)
                lviPlanes.Items.Add(new ListViewItem(new[] {s[1, i], s[2, i], s[3, i], s[4, i], s[5, i]}, 0) {Tag = s[0, i]});
            lviPlanes.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lviPlanes.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void InitCities(int ID)
        {
            cbCity.SelectedIndex = -1;
            cbCity.Items.Clear();
            C.Clear();
            int n = Game.GetCountCitiesFromTo(ID, lviPlanes.SelectedItems[0].Tag.ToString());
            string[,] s = Game.GetCitiesFromTo(ID, lviPlanes.SelectedItems[0].Tag.ToString());
            for (int i = 0; i < n; i++)
            {
                C.Add(new City(s[0, i], s[1, i]));
                cbCity.Items.Add(s[1, i]);
            }
            if (cbCity.Items.Count != 0)
                cbCity.SelectedIndex = 0;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void lviPlanes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lviPlanes.SelectedItems.Count != 0)
            {
                ss = Game.GetExpenseAndCity(lviPlanes.SelectedItems[0].Tag.ToString());
                lblCity.Text = ss[1];
                lblSam.Text = lviPlanes.SelectedItems[0].SubItems[0].Text;
                InitCities(int.Parse(ss[2]));
            }
        }

        private void cbCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lviPlanes.SelectedItems.Count != 0 && cbCity.SelectedIndex != -1)
            {
                if (cbCity.SelectedItem.ToString() != lblCity.Text)
                {
                    lblDist.Text = (Game.GetDistance(lviPlanes.SelectedItems[0].Tag.ToString(), C[cbCity.SelectedIndex].ID)).ToString();
                    lblCost.Text = (double.Parse(ss[0]) * int.Parse(lblDist.Text.ToString())).ToString();
                    lblTime.Text = (Game.GetTimeGoTo(lviPlanes.SelectedItems[0].Tag.ToString(), C[cbCity.SelectedIndex].ID)).ToString() + " минут";
                    COST = double.Parse(ss[0]) * int.Parse(lblDist.Text.ToString());
                }
            }
        }

        private void btnSand_Click(object sender, EventArgs e)
        {
            if (lviPlanes.SelectedItems.Count != 0)
            {
                Game.AddFlightsPlane(lblCity.Text, C[cbCity.SelectedIndex].ID, (-1) * Convert.ToInt32(COST), lviPlanes.SelectedItems[0].Tag.ToString());
                MessageBox.Show("Вы отправили " + lblSam.Text + " в город " + cbCity.SelectedItem);
                InitPlanes();
            }
            else
                MessageBox.Show("Сначала выберите самолет");
        }
    }

    public partial class City
    {
        public int ID;
        public string Name;

        public City(string id, string s)
        {
            ID = int.Parse(id);
            Name = s;
        }
    }
}