﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flight_Simulator
{
    //базовых класс всех самолетов
    class Airplane
    {
        public double Cost { get; set; } //стоимость самолета
        public string Name { get; set; } //название самолета
        public double Distance { get; set; } //максимальная дальность перелета
        public double CostOfService { get; set; } //стоимость обслуживания за 1 км

        public Airplane(double cost, string name, double distance, double costOfServise)
        {
            Cost = cost;
            Name = name;
            Distance = distance;
            CostOfService = costOfServise;
        }
    }

    //класс пассажирских самолетов
    class PassengerAirplane : Airplane
    {
        private int NumberOfSeats; //число мест

        public PassengerAirplane(double cost, string name, double distance, double costOfServise, int numberOfSeats) : base(cost, name, distance, costOfServise)
        {
            NumberOfSeats = numberOfSeats;
        }
    }

    //класс грузовых самолетов
    class CargoAirplane : Airplane
    {
        private double Carrying; //грузоподъемность

        public CargoAirplane(double cost, string name, double distance, double costOfServise, double carrying) : base(cost, name, distance, costOfServise)
        {
            Carrying = carrying;
        }
    }
}