﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flight_Simulator
{
    class ListDate
    {
        public DateTime Date;
        public int id;
        public ListDate (int i, DateTime d)
        {
            Date = d;
            id = i;
        }
    }
    class ListComparer : IComparer<ListDate>
    {
        public int Compare(ListDate x, ListDate y)
        {
            int returnVal = -1;
            DateTime x1 = x.Date;
            DateTime y1 = y.Date;
            if (x1 > y1)
                returnVal = -1;
            else
                if (x1 < y1)
                returnVal = 1;
            else
                returnVal = 0;
            return returnVal * (-1);
        }
    }
}
