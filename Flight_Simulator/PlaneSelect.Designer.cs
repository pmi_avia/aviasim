﻿namespace Flight_Simulator
{
    partial class PlaneSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAppointPlane = new System.Windows.Forms.Button();
            this.lviPlanes = new System.Windows.Forms.ListView();
            this.PlaneName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PlaneDistance = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PlaneStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // btnAppointPlane
            // 
            this.btnAppointPlane.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAppointPlane.Location = new System.Drawing.Point(3, 226);
            this.btnAppointPlane.Name = "btnAppointPlane";
            this.btnAppointPlane.Size = new System.Drawing.Size(279, 23);
            this.btnAppointPlane.TabIndex = 1;
            this.btnAppointPlane.Text = "Назначить самолет";
            this.btnAppointPlane.UseVisualStyleBackColor = true;
            this.btnAppointPlane.Click += new System.EventHandler(this.btnAppointPlane_Click);
            // 
            // lviPlanes
            // 
            this.lviPlanes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.PlaneName,
            this.PlaneDistance,
            this.PlaneStatus});
            this.lviPlanes.FullRowSelect = true;
            this.lviPlanes.Location = new System.Drawing.Point(3, 2);
            this.lviPlanes.MultiSelect = false;
            this.lviPlanes.Name = "lviPlanes";
            this.lviPlanes.Size = new System.Drawing.Size(279, 218);
            this.lviPlanes.TabIndex = 2;
            this.lviPlanes.UseCompatibleStateImageBehavior = false;
            this.lviPlanes.View = System.Windows.Forms.View.Details;
            // 
            // PlaneName
            // 
            this.PlaneName.Text = "Название";
            // 
            // PlaneDistance
            // 
            this.PlaneDistance.Text = "Дистанция";
            // 
            // PlaneStatus
            // 
            this.PlaneStatus.Text = "Статус";
            // 
            // PlaneSelect
            // 
            this.AcceptButton = this.btnAppointPlane;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 253);
            this.Controls.Add(this.lviPlanes);
            this.Controls.Add(this.btnAppointPlane);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PlaneSelect";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Назначить самолет";
            this.Load += new System.EventHandler(this.PlaneSelect_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAppointPlane;
        private System.Windows.Forms.ListView lviPlanes;
        private System.Windows.Forms.ColumnHeader PlaneName;
        private System.Windows.Forms.ColumnHeader PlaneDistance;
        private System.Windows.Forms.ColumnHeader PlaneStatus;
    }
}