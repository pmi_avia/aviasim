﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Flight_Simulator
{
    public partial class HangarForm : Form
    {
        public Game Game;
        public int SecondsToAdd = 1;
        public HangarForm(Game game)
        {
            InitializeComponent();
            Game = game;
            InitPlanes();
            lblMoney.Text = Game.GetMoneyStr();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void HangarForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            e.Cancel = true;
        }

        private void InitPlanes()
        {
            lviPlanes.Items.Clear();
            int n = Game.GetCountMyPlanes();
            string[,] s = Game.GetMyPlanes();

            for (int i = 0; i < n; i++)
            {
                if(s[10,i]!="")
                    lviPlanes.Items.Add(new ListViewItem(new[] { s[1, i], s[2, i], s[4, i], s[5, i], s[6, i], s[7, i],  s[8, i], "-", s[9, i], s[10, i] }, 0) { Tag = s[0, i] });
                else
                {
                    lviPlanes.Items.Add(new ListViewItem(new[] { s[1, i], s[2, i], s[4, i], s[5, i], s[6, i], s[7, i], s[8, i], (double.Parse(s[3, i]) / 2).ToString(), s[9, i] }, 0) { Tag = s[0, i] });
                }
            }
            lviPlanes.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lviPlanes.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

        }

        private void timerToTime_Tick(object sender, EventArgs e)
        {
            lblTime.Text = Game.GetTime.ToString();
        }

        private void btnSell_Click(object sender, EventArgs e)
        {
            if (lviPlanes.SelectedItems.Count == 0) //если юзер не выбрал самолет
            {
                MessageBox.Show("Сначала выберите самолет!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (lviPlanes.SelectedItems[0].SubItems[7].Text == "-")
            {
                MessageBox.Show("Вы не можете продать этот самолет! Он находится либо в лизинге либо в аренде!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (MessageBox.Show("Вы действительно хотите продать этот самолет?",
                    "Подтверждение продажи", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Game.DeleteMyPlane((lviPlanes.SelectedItems[0].Tag).ToString(), int.Parse(lviPlanes.SelectedItems[0].SubItems[7].Text));
                lblMoney.Text = Game.GetMoneyStr();
                lviPlanes.Items.Remove(lviPlanes.SelectedItems[0]);
            }
        }

        private void HangarForm_Load(object sender, EventArgs e)
        {
            InitPlanes();
            lblMoney.Text = Game.GetMoneyStr();
        }

        private void lviPlanes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lviPlanes.SelectedItems.Count != 0)
            {
                if(lviPlanes.SelectedItems[0].SubItems[6].Text == "-")
                    btnSell.Enabled = false;
                else
                    btnSell.Enabled = true;
            }
        }
    }
}