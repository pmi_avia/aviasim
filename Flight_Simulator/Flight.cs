﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Flight_Simulator
{
    //базовый класс всех рейсов
    class Flight
    {
        public string Name { get; set; } //название рейса
        public string Airplane { get; set; } //название самолета
        public DateTime DepartureTime { get; set; } //время отправления
        public DateTime ArrivalTime { get; set; } //время прибытия
        public string DeparturePlace { get; set; } //место отправления
        public string ArrivalPlace { get; set; } //место прибытия

        public Flight(string name, string airplane, DateTime departureTime, DateTime arrivalTime, string departurePlace, string arrivalPlace)
        {
            Name = name;
            Airplane = airplane;
            DepartureTime = departureTime;
            ArrivalTime = arrivalTime;
            DeparturePlace = departurePlace;
            ArrivalPlace = arrivalPlace;
        }
    }

    //класс пассажирских рейсов
    class PassengerFlight : Flight
    {
        private double TicketCost; //стоимость билета
        private int NumberOfPassengers; //количество пассажиров

        public PassengerFlight(string name, string airplane, DateTime departureTime, DateTime arrivalTime, string departurePlace, string arrivalPlace, double ticketCost, int numberOfPassengers) : base(name, airplane, departureTime, arrivalTime, departurePlace, arrivalPlace)
        {
            TicketCost = ticketCost;
            NumberOfPassengers = numberOfPassengers;
        }

        //доход от рейса
        public double Income()
        {
            return TicketCost * NumberOfPassengers;
        }
    }

    //класс грузовых рейсов
    class CargoFlight : Flight
    {
        private double Income; //доход от рейса

        public CargoFlight(string name, string airplane, DateTime departureTime, DateTime arrivalTime, string departurePlace, string arrivalPlace, double income) : base(name, airplane, departureTime, arrivalTime, departurePlace, arrivalPlace)
        {
            Income = income;
        }
    }
}