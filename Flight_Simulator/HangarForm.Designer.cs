﻿namespace Flight_Simulator
{
    partial class HangarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSell = new System.Windows.Forms.Button();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblMoney = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.lviPlanes = new System.Windows.Forms.ListView();
            this.PlaneName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.place = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Carrying = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PasCapacity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Distance = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Tenancy = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Expense = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Sell = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DateBeg = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DateEnd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timerToTime = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSell
            // 
            this.btnSell.Enabled = false;
            this.btnSell.Location = new System.Drawing.Point(12, 333);
            this.btnSell.Name = "btnSell";
            this.btnSell.Size = new System.Drawing.Size(94, 23);
            this.btnSell.TabIndex = 1;
            this.btnSell.Text = "Продать";
            this.btnSell.UseVisualStyleBackColor = true;
            this.btnSell.Click += new System.EventHandler(this.btnSell_Click);
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblTime.Location = new System.Drawing.Point(46, 0);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(40, 13);
            this.lblTime.TabIndex = 2;
            this.lblTime.Text = "lblTime";
            // 
            // lblMoney
            // 
            this.lblMoney.AutoSize = true;
            this.lblMoney.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblMoney.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblMoney.Location = new System.Drawing.Point(828, 0);
            this.lblMoney.Name = "lblMoney";
            this.lblMoney.Size = new System.Drawing.Size(49, 13);
            this.lblMoney.TabIndex = 3;
            this.lblMoney.Text = "lblMoney";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(755, 333);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "OK";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lviPlanes
            // 
            this.lviPlanes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.PlaneName,
            this.place,
            this.Carrying,
            this.PasCapacity,
            this.Distance,
            this.Tenancy,
            this.Expense,
            this.Sell,
            this.DateBeg,
            this.DateEnd});
            this.lviPlanes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lviPlanes.FullRowSelect = true;
            this.lviPlanes.GridLines = true;
            this.lviPlanes.Location = new System.Drawing.Point(0, 0);
            this.lviPlanes.MultiSelect = false;
            this.lviPlanes.Name = "lviPlanes";
            this.lviPlanes.Size = new System.Drawing.Size(877, 308);
            this.lviPlanes.TabIndex = 5;
            this.lviPlanes.UseCompatibleStateImageBehavior = false;
            this.lviPlanes.View = System.Windows.Forms.View.Details;
            this.lviPlanes.SelectedIndexChanged += new System.EventHandler(this.lviPlanes_SelectedIndexChanged);
            // 
            // PlaneName
            // 
            this.PlaneName.Text = "Название";
            // 
            // place
            // 
            this.place.Text = "Нахождение";
            // 
            // Carrying
            // 
            this.Carrying.Text = "Груз-ть";
            // 
            // PasCapacity
            // 
            this.PasCapacity.Text = "Кол-во пассажиров";
            this.PasCapacity.Width = 118;
            // 
            // Distance
            // 
            this.Distance.Text = "Дальность";
            // 
            // Tenancy
            // 
            this.Tenancy.Text = "Аренда в месяц";
            // 
            // Expense
            // 
            this.Expense.Text = "Издержки";
            // 
            // Sell
            // 
            this.Sell.Text = "Продажа";
            // 
            // DateBeg
            // 
            this.DateBeg.Text = "Дата начала эксплуатации";
            // 
            // DateEnd
            // 
            this.DateEnd.Text = "Дата конца эксплуатации";
            // 
            // timerToTime
            // 
            this.timerToTime.Enabled = true;
            this.timerToTime.Interval = 1000;
            this.timerToTime.Tick += new System.EventHandler(this.timerToTime_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblMoney);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(877, 19);
            this.panel1.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Location = new System.Drawing.Point(780, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Деньги:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Время: ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lviPlanes);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(877, 308);
            this.panel2.TabIndex = 7;
            // 
            // HangarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 366);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSell);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HangarForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ангар";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HangarForm_FormClosing);
            this.Load += new System.EventHandler(this.HangarForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnSell;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblMoney;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ListView lviPlanes;
        private System.Windows.Forms.ColumnHeader PlaneName;
        private System.Windows.Forms.ColumnHeader Carrying;
        private System.Windows.Forms.ColumnHeader PasCapacity;
        private System.Windows.Forms.ColumnHeader Distance;
        private System.Windows.Forms.ColumnHeader Tenancy;
        private System.Windows.Forms.ColumnHeader Expense;
        private System.Windows.Forms.ColumnHeader Sell;
        private System.Windows.Forms.Timer timerToTime;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ColumnHeader DateBeg;
        private System.Windows.Forms.ColumnHeader DateEnd;
        private System.Windows.Forms.ColumnHeader place;
    }
}